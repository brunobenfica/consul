#Consul ACL Configuration
crontab -r
(crontab -l 2>/dev/null || true; echo "@reboot sleep 30 && consul agent -config-dir /root/consul/") | crontab -
rm /root/consul/config.json
mv /root/consul/config.json.new /root/consul/config.json
consul acl bootstrap > /root/consul/token.txt
export VAR_ACCESSORID=$(cat /root/consul/token.txt | grep AccessorID | cut -c19-54)
export VAR_SECRETID=$(cat /root/consul/token.txt | grep SecretID | cut -c19-54)
export VAR_IPADDRESS=$(cat /root/consul/ip1.txt)
consul acl token create -accessor 2f829854-532b-1923-3bb5-14fb334411da -secret 1cf192c1-c4a1-c444-e90c-9fc2d4757936 -policy-name global-management -token=$VAR_SECRETID
consul acl token delete -id $VAR_ACCESSORID -token=1cf192c1-c4a1-c444-e90c-9fc2d4757936
consul kv put -token=1cf192c1-c4a1-c444-e90c-9fc2d4757936 consul/config/ip1 $VAR_IPADDRESS
#rm /root/consul/token.txt
#rm /root/consul/ip1.txt
#rm /root/consul/consul_acl.sh
#shutdown -r 15