crontab -r
(crontab -l 2>/dev/null || true; echo "@reboot sleep 30 && consul agent -config-dir /root/consul/") | crontab -
export VAR_IPADDRESS=$(cat /root/consul/ip1.txt) 
consul agent -config-dir /root/consul/ -retry-join=$VAR_IPADDRESS
shutdown -r 2